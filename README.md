## Simple Spreadsheet ##

---

###This program can do work as follows:  ###
* Create a new spread sheet  
* Add numbers in different cells and perform some calculation on top of specific row or column  
* Quit  

---

###Development environ and runtime  ###
* This application is developed with **Visual Studio 2015 C#**  
* Supported runtime **.NETFramework v4.5.2**  
---
###File List  ###
        ./
        │  .gitignore
        │  README.md
        │  SimpleSpreadsheet.sln
        │  
        ├─SimpleSpreadsheet
        │  │  App.config
        │  │  Program.cs
        │  │  SimpleSpreadsheet.csproj
        │  │  
        │  ├─bin
        │  │  └─Release
        │  │          SimpleSpreadsheet.exe
        │  │          SimpleSpreadsheet.exe.config
        │  │          
        │  ├─Control
        │  │      SheetCell.cs
        │  │      SheetException.cs
        │  │      SheetRange.cs
        │  │      SpreadSheet.cs
        │  │      
        │  └─Properties
        │          AssemblyInfo.cs
        │          
        ├─SimpleSpreadsheet.TestCase
        │  │  ProgramTests.cs
        │  │  SheetRangeTest.cs
        │  │  SimpleSpreadsheet.TestCase.csproj
        │  │  SpreadSheetTest.cs
        │  │  
        │  ├─bin
        │  │  └─Release
        │  │          SimpleSpreadsheet.exe
        │  │          SimpleSpreadsheet.TestCase.dll
        │  │          
        │  └─Properties
        │          AssemblyInfo.cs
        │          
        └─TestResults
            └─02d80a75-8167-4116-9346-71db8d72846e
                    caojq_DESKTOP-4EM7DNK 2018-05-23 10_40_17.coverage

---

###Running  ###
* This is a windows console application.  
* Application running file :  /SimpleSpreadsheet/bin/Release/SimpleSpreadsheet.exe  
* You can running this application by double click,  and you can also running it at command line.  
* Input your command for create a new spread sheet , for add numbers in different cells and perform some  
        calculation on top of specific row or column, for Quit.
* You can enter the following commands:  
    1. Create a new spread: **`C w h`**  
    2. Insert a number in specified cell (x1,y1): **`N x1 y1 v1`**  
    3. Sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3: **`S x1 y1 x2 y2 x3 y3`**  
    4. Quit: **`Q`**  

---

### Coverage Info  ###

| Hierachy | NotCovered(Blocks) | NotCovered(% Blocks) | Covered(Blocks) | Covered(% Blocks)  | 
|-|-:|-:|-:|-:| 
|caojq_DESKTOP-4EM7DNK 2018-05-23 10_40_17.coverage|45|10.04%|403|89.96%|
|simplespreadsheet.exe|30|13.04%|200|86.96%|
|SimpleSpreadsheet|29|34.94%|54|65.06%|
|Program|29|34.94%|54|65.06%|
|CommandToRun(string)|1|1.89%|52|98.11%|
|Main()|28|100.00%|0|0.00%|
|get_Sheet()|0|0.00%|1|100.00%|
|set_Sheet(SimpleSpreadsheet.Control.SpreadSheet)|0|0.00%|1|100.00%|
|SimpleSpreadsheet.Control|1|0.68%|146|99.32%|
|SheetCell|0|0.00%|18|100.00%|
|ToString()|0|0.00%|2|100.00%|
|get_Colnum()|0|0.00%|1|100.00%|
|get_DataFormat()|0|0.00%|1|100.00%|
|get_Row()|0|0.00%|1|100.00%|
|get_Text()|0|0.00%|9|100.00%|
|get_Value()|0|0.00%|1|100.00%|
|set_Colnum(int)|0|0.00%|1|100.00%|
|set_Row(int)|0|0.00%|1|100.00%|
|set_Value(System.Nullable<int>)|0|0.00%|1|100.00%|
|SheetException|1|5.56%|17|94.44%|
|GetMessage(int)|1|16.67%|5|83.33%|
|InitErrorMsg()|0|0.00%|8|100.00%|
|SheetException(int)|0|0.00%|3|100.00%|
|get_ErrNum()|0|0.00%|1|100.00%|
|SheetRange|0|0.00%|11|100.00%|
|SheetRange(int, int, int, int)|0|0.00%|5|100.00%|
|get_Column()|0|0.00%|1|100.00%|
|get_Column2()|0|0.00%|1|100.00%|
|get_Columns()|0|0.00%|1|100.00%|
|get_Row()|0|0.00%|1|100.00%|
|get_Row2()|0|0.00%|1|100.00%|
|get_Rows()|0|0.00%|1|100.00%|
|SpreadSheet|0|0.00%|100|100.00%|
|CreateSheet(int, int)|0|0.00%|2|100.00%|
|GetCell(int, int)|0|0.00%|3|100.00%|
|GetFooterString()|0|0.00%|2|100.00%|
|GetHeaderString()|0|0.00%|2|100.00%|
|GetRowString(int)|0|0.00%|19|100.00%|
|IsOutOfRange(System.Tuple<int, int>[])|0|0.00%|6|100.00%|
|IsOutOfRange(int, int)|0|0.00%|7|100.00%|
|Reset(int, int)|0|0.00%|6|100.00%|
|SetCell(int, int, System.Nullable<int>)|0|0.00%|10|100.00%|
|SpreadSheet(int, int)|0|0.00%|3|100.00%|
|SumRange(int, int, int, int)|0|0.00%|21|100.00%|
|ToString()|0|0.00%|12|100.00%|
|get_Item(int, int)|0|0.00%|5|100.00%|
|set_Item(int, int, System.Nullable<int>)|0|0.00%|2|100.00%|
|simplespreadsheet.testcase.dll|15|6.88%|203|93.12%|
|SimpleSpreadsheet.TestCase|1|3.33%|29|96.67%|
|SheetRangeTest|0|0.00%|6|100.00%|
|TestSheetRangeRowAndColumns()|0|0.00%|6|100.00%|
|SpreadSheetTest|1|4.17%|23|95.83%|
|GetRowStringTest()|1|4.17%|23|95.83%|
|SimpleSpreadsheet.Tests|14|7.45%|174|92.55%|
|ProgramTests|14|7.45%|174|92.55%|
|CommandNotDefined()|2|28.57%|5|71.43%|
|CommandParamsNotEnough()|2|22.22%|7|77.78%|
|CommandParamsTooMany()|0|0.00%|43|100.00%|
|CommandRunTestForInsertDataToCell()|0|0.00%|24|100.00%|
|CommandRunTestForQuit()|0|0.00%|3|100.00%|
|CommandRunTestForSumData()|0|0.00%|47|100.00%|
|CommandToRunTestForCreateSheet()|0|0.00%|14|100.00%|
|InsertDataBeforeCreateSheet()|2|28.57%|5|71.43%|
|RowsOrColumnsIsOutOfRange_01()|2|22.22%|7|77.78%|
|RowsOrColumnsIsOutOfRange_02()|2|22.22%|7|77.78%|
|RowsOrColumnsLessThan1()|2|28.57%|5|71.43%|
|SumDataBeforeCreateSheet()|2|28.57%|5|71.43%|
|TestInitialize()|0|0.00%|2|100.00%|
