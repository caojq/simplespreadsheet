﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet.Control;

namespace SimpleSpreadsheet.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        #region TestInitialize

        /// <summary>
        /// Identifies the method to run before the test to allocate and configure resources
        /// needed by all tests in the test class. This class cannot be inherited.
        /// </summary>
        [TestInitialize()]
        public void TestInitialize()
        {
            // remove the sheet
            Program.Sheet = null;
        }

        #endregion TestInitialize

        #region Normal Test

        /// <summary>
        /// Test create a new spread sheet of width w and height h
        /// </summary>
        [TestMethod()]
        public void CommandToRunTestForCreateSheet()
        {
            // Command is C w h
            // Create a sheet by 10 columns and 5 rows
            var cmdText = "C 10 5";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // check the sheet has been created and the datas in all cells is null
            Assert.IsNotNull(Program.Sheet != null);
            for (int colIndex = 1; colIndex <= 10; colIndex++)
            {
                for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
                {
                    Assert.IsNull(Program.Sheet[rowIndex, colIndex]);
                }
            }
        }

        /// <summary>
        /// Test insert a number in specified cell (x1,y1)
        /// </summary>
        [TestMethod]
        public void CommandRunTestForInsertDataToCell()
        {
            // Command is C w h
            // Create a sheet by 10 columns and 5 rows
            var cmdText = "C 10 5";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // insert a number in specified cell (6 , 3)
            // Command is N 6 3 2
            cmdText = "N 6 3 2";
            cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // check the sheet has been created and the datas in all cells is null except specified cell (6 , 3)
            for (int colIndex = 1; colIndex <= 10; colIndex++)
            {
                for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
                {
                    if (colIndex == 6 && rowIndex == 3)
                    {
                        Assert.IsNotNull(Program.Sheet[rowIndex, colIndex]);
                        Assert.AreEqual<int>(2, Program.Sheet[rowIndex, colIndex].Value);
                    }
                    else
                    {
                        Assert.IsNull(Program.Sheet[rowIndex, colIndex]);
                    }
                }
            }
        }

        /// <summary>
        /// Test perform sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3
        /// </summary>
        [TestMethod]
        public void CommandRunTestForSumData()
        {
            // Command is C w h
            // Create a sheet by 10 columns and 5 rows
            var cmdText = "C 10 5";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // insert the data in sheet as
            /*
             * --------------------------------
             * |  1  2  3  4  5  6  7  8  9 10|
             * | 11 12 13 14 15 16 17 18 19 20|
             * | 21 22 23 24 25 26 27 28 29 30|
             * | 31 32 33 34 35 36 37 38 39 40|
             * | 41 42 43 44 45 46 47 48 49 50|
             * --------------------------------
             */
            int cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    cmdText = string.Format("N {0} {1} {2}", colIndex, rowIndex, cellData++);
                    cmdResult = Program.CommandToRun(cmdText);
                    Assert.AreEqual(CmdResult.Normal, cmdResult);
                }
            }

            // check all data in cells
            cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    Assert.AreEqual(cellData++, Program.Sheet[rowIndex, colIndex]);
                }
            }

            // Sum the cell from [2, 2] to [9, 4] and store the result in  [10, 5] as result:
            /*
             * --------------------------------
             * |  1  2  3  4  5  6  7  8  9 10|
             * | 11 12 13 14 15 16 17 18 19 20|
             * | 21 22 23 24 25 26 27 28 29 30|
             * | 31 32 33 34 35 36 37 38 39 40|
             * | 41 42 43 44 45 46 47 48 49612|
             * --------------------------------
             */

            // Command is S 2 2 7 4 10 5
            cmdText = "S 2 2 9 4 10 5";
            cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // check all data in cells
            cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    if (colIndex == 10 && rowIndex == 5)
                    {
                        Assert.AreEqual(612, Program.Sheet[rowIndex, colIndex].Value);
                    }
                    else
                    {
                        Assert.AreEqual(cellData++, Program.Sheet[rowIndex, colIndex].Value);
                    }

                    var cell = Program.Sheet.GetCell(rowIndex, colIndex);
                    Assert.AreEqual(rowIndex, cell.Row);
                    Assert.AreEqual(colIndex, cell.Colnum);
                }
            }
        }

        /// <summary>
        /// Test application exit
        /// </summary>
        [TestMethod]
        public void CommandRunTestForQuit()
        {
            // Command is Q
            var cmdText = "Q";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Exit, cmdResult);
        }

        #endregion Normal Test

        #region Exception Test

        /// <summary>
        /// Test insert data before create sheet
        /// </summary>
        [TestMethod]
        public void InsertDataBeforeCreateSheet()
        {
            try
            {
                // insert a number in specified cell (6 , 3) before create the sheet
                // Command is N 6 3 2
                var cmdText = "N 6 3 2";
                var cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-995, sex.ErrNum);
            }
        }

        /// <summary>
        /// Test sum data before create sheet
        /// </summary>
        [TestMethod]
        public void SumDataBeforeCreateSheet()
        {
            try
            {
                // Sum the cell from [2, 2] to [9, 4] and store the result in  [10, 5] as result:
                // Command is S 2 2 7 4 10 5
                var cmdText = "S 2 2 9 4 10 5";
                var cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-995, sex.ErrNum);
            }
        }

        /// <summary>
        /// Test the command is not defined
        /// </summary>
        [TestMethod]
        public void CommandNotDefined()
        {
            try
            {
                // Command is CS 1 1
                var cmdText = " CS 1 1";
                var cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-900, sex.ErrNum);
            }
        }

        /// <summary>
        /// Test the parameters of command is not enough
        /// </summary>
        [TestMethod]
        public void CommandParamsNotEnough()
        {
            try
            {
                // Command is C w h
                // Create a sheet by 10 columns and 5 rows
                var cmdText = "C 10 5";
                var cmdResult = Program.CommandToRun(cmdText);
                Assert.AreEqual(CmdResult.Normal, cmdResult);

                // insert a number in specified cell (6 , 3)
                // Command is N 6 3
                cmdText = "N 6 3";
                cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-900, sex.ErrNum);
            }
        }

        /// <summary>
        /// Test the parameters of command is too many
        /// </summary>
        [TestMethod()]
        public void CommandParamsTooMany()
        {
            // Command is C w h
            // Create a sheet by 10 columns and 5 rows
            var cmdText = "C 10 5 99";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // insert the data in sheet as
            /*
             * --------------------------------
             * |  1  2  3  4  5  6  7  8  9 10|
             * | 11 12 13 14 15 16 17 18 19 20|
             * | 21 22 23 24 25 26 27 28 29 30|
             * | 31 32 33 34 35 36 37 38 39 40|
             * | 41 42 43 44 45 46 47 48 49 50|
             * --------------------------------
             */
            int cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    cmdText = string.Format("N {0} {1} {2} 99", colIndex, rowIndex, cellData++);
                    cmdResult = Program.CommandToRun(cmdText);
                    Assert.AreEqual(CmdResult.Normal, cmdResult);
                }
            }

            // check all data in cells
            cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    Assert.AreEqual(cellData++, Program.Sheet[rowIndex, colIndex]);
                }
            }

            // Sum the cell from [2, 2] to [9, 4] and store the result in  [10, 5] as result:
            /*
             * --------------------------------
             * |  1  2  3  4  5  6  7  8  9 10|
             * | 11 12 13 14 15 16 17 18 19 20|
             * | 21 22 23 24 25 26 27 28 29 30|
             * | 31 32 33 34 35 36 37 38 39 40|
             * | 41 42 43 44 45 46 47 48 49612|
             * --------------------------------
             */

            // Command is S 2 2 7 4 10 5
            cmdText = "S 2 2 9 4 10 5 99";
            cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // check all data in cells
            cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    if (colIndex == 10 && rowIndex == 5)
                    {
                        Assert.AreEqual(612, Program.Sheet[rowIndex, colIndex].Value);
                    }
                    else
                    {
                        Assert.AreEqual(cellData++, Program.Sheet[rowIndex, colIndex].Value);
                    }
                }
            }

            // Command is Q
            cmdText = "Q 2 2 9 4 10 5 99";
            cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Exit, cmdResult);
        }

        /// <summary>
        /// The rows or columns is less than 1.
        /// </summary>
        [TestMethod()]
        public void RowsOrColumnsLessThan1()
        {
            try
            {
                // Command is C w h
                // Create a sheet by 0 columns and 0 rows
                var cmdText = "C 0 0";
                var cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-999, sex.ErrNum);
            }
        }

        /// <summary>
        /// The rows or columns is out of range.
        /// </summary>
        [TestMethod()]
        public void RowsOrColumnsIsOutOfRange_01()
        {
            try
            {
                // Command is C w h
                // Create a sheet by 10 columns and 5 rows
                var cmdText = "C 10 5";
                var cmdResult = Program.CommandToRun(cmdText);
                Assert.AreEqual(CmdResult.Normal, cmdResult);

                // insert a number in specified cell (11 , 3)
                // Command is N 11 3 2
                cmdText = "N 11 3 2";
                cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-998, sex.ErrNum);
            }
        }

        /// <summary>
        /// The rows or columns is out of range.
        /// </summary>
        [TestMethod()]
        public void RowsOrColumnsIsOutOfRange_02()
        {
            try
            {
                // Command is C w h
                // Create a sheet by 10 columns and 5 rows
                var cmdText = "C 10 5";
                var cmdResult = Program.CommandToRun(cmdText);
                Assert.AreEqual(CmdResult.Normal, cmdResult);

                // insert a number in specified cell (11 , 3)
                // Command is N 11 3 2
                cmdText = "N 10 6 2";
                cmdResult = Program.CommandToRun(cmdText);

                // no exception is fail
                Assert.Fail();
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-998, sex.ErrNum);
            }
        }

        #endregion Exception Test
    }
}