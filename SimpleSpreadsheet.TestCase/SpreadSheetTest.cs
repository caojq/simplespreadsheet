﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet.Control;

namespace SimpleSpreadsheet.TestCase
{
    /// <summary>
    /// special test for Spread Sheet
    /// </summary>
    [TestClass]
    public class SpreadSheetTest
    {
        /// <summary>
        /// Test GetRowString
        /// </summary>
        [TestMethod]
        public void GetRowStringTest()
        {
            // Command is C w h
            // Create a sheet by 10 columns and 5 rows
            var cmdText = "C 10 5";
            var cmdResult = Program.CommandToRun(cmdText);
            Assert.AreEqual(CmdResult.Normal, cmdResult);

            // insert the data in sheet as
            /*
             * --------------------------------
             * |  1  2  3  4  5  6  7  8  9 10|
             * | 11 12 13 14 15 16 17 18 19 20|
             * | 21 22 23 24 25 26 27 28 29 30|
             * | 31 32 33 34 35 36 37 38 39 40|
             * | 41 42 43 44 45 46 47 48 49 50|
             * --------------------------------
             */
            int cellData = 1;
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                for (int colIndex = 1; colIndex <= 10; colIndex++)
                {
                    cmdText = string.Format("N {0} {1} {2}", colIndex, rowIndex, cellData++);
                    cmdResult = Program.CommandToRun(cmdText);
                    Assert.AreEqual(CmdResult.Normal, cmdResult);
                }
            }

            // rows string
            var rowStrings = new string[] {
                "|  1  2  3  4  5  6  7  8  9 10|",
                "| 11 12 13 14 15 16 17 18 19 20|",
                "| 21 22 23 24 25 26 27 28 29 30|",
                "| 31 32 33 34 35 36 37 38 39 40|",
                "| 41 42 43 44 45 46 47 48 49 50|"
            };

            // check rows string
            for (int rowIndex = 1; rowIndex <= 5; rowIndex++)
            {
                Assert.AreEqual(rowStrings[rowIndex - 1], Program.Sheet.GetRowString(rowIndex));
            }

            // row is out of range
            try
            {
                var rowString = Program.Sheet.GetRowString(0);
            }
            catch (SheetException sex)
            {
                Assert.AreEqual(-997, sex.ErrNum);
            }
        }
    }
}