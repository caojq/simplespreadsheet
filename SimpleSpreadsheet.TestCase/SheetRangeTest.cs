﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet.Control;

namespace SimpleSpreadsheet.TestCase
{
    /// <summary>
    /// special test for SheetRange
    /// </summary>
    [TestClass]
    public class SheetRangeTest
    {
        [TestMethod]
        public void TestSheetRangeRowAndColumns()
        {
            var range = new SheetRange(1, 1, 5, 10);
            Assert.AreEqual(5, range.Rows);
            Assert.AreEqual(10, range.Columns);
        }
    }
}