﻿using System;
using System.Text;

namespace SimpleSpreadsheet.Control
{
    /// <summary>
    /// Simple SpreadSheet Control
    /// </summary>
    public class SpreadSheet
    {
        #region Fields

        /// <summary>
        /// Rows of sheet. (minimum is 1)
        /// </summary>
        private int _rows;

        /// <summary>
        /// Columns of range. (minimum is 1)
        /// </summary>
        private int _columns;

        /// <summary>
        /// Cells in the sheet
        /// </summary>
        private SheetCell[,] _cells = null;

        #endregion Fields

        #region Get data of cell in specified position

        /// <summary>
        /// Get data of cell in specified position
        /// </summary>
        /// <param name="row">Index of row of specified cell</param>
        /// <param name="column">Index of column of specified cell</param>
        /// <returns>Data of cell in specified position</returns>
        public int? this[int row, int column]
        {
            get
            {
                var cell = this.GetCell(row, column);
                return cell == null ? null : cell.Value;
            }
            set
            {
                this.SetCell(row, column, value);
            }
        }

        #endregion Get data of cell in specified position

        #region Create sheet instance

        /// <summary>
        /// Create sheet instance  with rows and columns of sheet
        /// </summary>
        /// <param name="rows">rows of sheet</param>
        /// <param name="columns">columns of sheet</param>
        /// <returns>sheet instance created</returns>
        public static SpreadSheet CreateSheet(int rows, int columns)
        {
            return new SpreadSheet(rows, columns);
        }

        /// <summary>
        /// Create instance with rows and columns of sheet
        /// </summary>
        /// <param name="rows">rows of sheet</param>
        /// <param name="columns">columns of sheet</param>
        private SpreadSheet(int rows, int columns)
        {
            this.Reset(rows, columns);
        }

        #endregion Create sheet instance

        #region Reset the rows and columns of this sheet, and clear all data

        /// <summary>
        /// Reset the rows and columns of this sheet, and clear all data
        /// </summary>
        /// <param name="rows">rows of sheet</param>
        /// <param name="columns">columns of sheet</param>
        public void Reset(int rows, int columns)
        {
            // Check the rows and columns is less than 1.
            if (rows < 1 || columns < 1)
            {
                throw new SheetException(-999);
            }

            this._rows = rows;
            this._columns = columns;
            this._cells = new SheetCell[rows + 1, columns + 1];
        }

        #endregion Reset the rows and columns of this sheet, and clear all data

        #region Retrieve cell at specified index of row and column.

        /// <summary>
        /// Retrieve cell at specified index of row and column.
        /// </summary>
        /// <param name="row">Index of row</param>
        /// <param name="column">Index of column</param>
        /// <returns>Return null if specified position has nothing.</returns>
        public SheetCell GetCell(int row, int column)
        {
            this.IsOutOfRange(row, column);
            return this._cells[row, column];
        }

        #endregion Retrieve cell at specified index of row and column.

        #region Set cell at specified index of row and column.

        /// <summary>
        /// Set cell at specified index of row and column.
        /// </summary>
        /// <param name="row">Index of row</param>
        /// <param name="column">Index of column</param>
        /// <param name="value">Value of the cell</param>
        private void SetCell(int row, int column, int? value)
        {
            this.IsOutOfRange(row, column);

            // Clear the data at specified index of row and column.
            this._cells[row, column] = null;
            if (value.HasValue)
            {
                this._cells[row, column] = new SheetCell()
                {
                    Row = row,
                    Colnum = column,
                    Value = value
                };
            }
        }

        #endregion Set cell at specified index of row and column.

        #region Check the specified position is out of sheet

        /// <summary>
        /// If the specified position is out of sheet then throw a exception.
        /// </summary>
        /// <param name="row">Index of row</param>
        /// <param name="column">Index of column</param>
        private void IsOutOfRange(int row, int column)
        {
            if (row < 1 || row > this._rows
                || column < 1 || column > this._columns)
            {
                throw new SheetException(-998);
            }
        }

        /// <summary>
        ///  If the specified position is out of sheet then throw a exception.
        /// </summary>
        /// <param name="pos">The specified position</param>
        private void IsOutOfRange(params Tuple<int, int>[] posList)
        {
            foreach (var itemPos in posList)
            {
                this.IsOutOfRange(itemPos.Item1, itemPos.Item2);
            }
        }

        #endregion Check the specified position is out of sheet

        #region Get a string representation of sheet header

        /// <summary>
        /// Get a string representation of sheet header
        /// </summary>
        /// <returns>The header of sheet</returns>
        public string GetHeaderString()
        {
            return "-".PadRight(this._columns * 3 + 2, '-');
        }

        #endregion Get a string representation of sheet header

        #region Get a string representation of the datas in row

        /// <summary>
        /// Get a string representation of the datas in row
        /// </summary>
        /// <param name="row">Index of row</param>
        /// <returns>A string representation of the datas in row </returns>
        public string GetRowString(int row)
        {
            // If the row is out of sheet then throw a exception.
            if (row < 1 || row > this._rows)
            {
                throw new SheetException(-997);
            }

            StringBuilder sbRowString = new StringBuilder(this._columns * 3 + 2);
            sbRowString.Append("|");
            for (int colIndex = 1; colIndex <= this._columns; colIndex++)
            {
                var cell = this.GetCell(row, colIndex);
                sbRowString.AppendFormat("{0}", cell == null ? (new SheetCell()).ToString() : cell.ToString());
            }
            sbRowString.Append("|");
            return sbRowString.ToString();
        }

        #endregion Get a string representation of the datas in row

        #region Get a string representation of sheet

        /// <summary>
        /// Get a string representation of sheet footer
        /// </summary>
        /// <returns>The footer of sheet</returns>
        public string GetFooterString()
        {
            return "-".PadRight(this._columns * 3 + 2, '-');
        }

        #endregion Get a string representation of sheet

        #region Creates and returns a string representation of the current sheet

        /// <summary>
        /// Creates and returns a string representation of the current sheet
        /// </summary>
        /// <returns>A string representation of the current sheet</returns>
        public override string ToString()
        {
            StringBuilder sbResult = new StringBuilder((this._columns * 3 + 2) * (this._rows + 2));

            // Add the header of sheet
            sbResult.AppendLine(this.GetHeaderString());

            // Add the datas of sheet
            for (int rowIndex = 1; rowIndex <= this._rows; rowIndex++)
            {
                sbResult.AppendLine(this.GetRowString(rowIndex));
            }

            // Add the footer of sheet
            sbResult.AppendLine(this.GetFooterString());

            // return the string representation of sheet
            return sbResult.ToString();
        }

        #endregion Creates and returns a string representation of the current sheet

        #region Sum the datas in specified range (from [row, column] to [row2, column2])

        /// <summary>
        /// Sum the datas in specified range (from [row, column] to [row2, column2])
        /// </summary>
        /// <param name="row">Index of start row</param>
        /// <param name="column">Index of start column</param>
        /// <param name="row2">Index of end row</param>
        /// <param name="column2">Index of end column</param>
        /// <returns>The sum result of the range</returns>
        public int SumRange(int row, int column, int row2, int column2)
        {
            this.IsOutOfRange(
                Tuple.Create(row, column),
                Tuple.Create(row2, column2)
            );

            // declare the result for return
            int result = 0;

            // set sum range
            var range = new SheetRange(row, column, row2, column2);

            // sum datas of cells in the range
            for (int rowIndex = range.Row; rowIndex <= range.Row2; rowIndex++)
            {
                for (int colIndex = range.Column; colIndex <= range.Column2; colIndex++)
                {
                    if (this[rowIndex, colIndex].HasValue)
                    {
                        result += this[rowIndex, colIndex].Value;
                    }
                }
            }

            // return sum result
            return result;
        }

        #endregion Sum the datas in specified range (from [row, column] to [row2, column2])
    }
}