﻿using System;

namespace SimpleSpreadsheet.Control
{
    /// <summary>
	/// Describe a range of cells on sheet.
	/// </summary>
    public struct SheetRange
    {
        #region The start row of range

        /// <summary>
        /// The start row of range
        /// </summary>
        private int _row;

        /// <summary>
        /// The start row of range
        /// </summary>
        public int Row
        {
            get
            {
                return this._row;
            }
        }

        #endregion The start row of range

        #region The start column of range

        /// <summary>
        /// The start column of range
        /// </summary>
        private int _column;

        /// <summary>
        /// The start column of range
        /// </summary>
        public int Column
        {
            get
            {
                return this._column;
            }
        }

        #endregion The start column of range

        #region Rows of range.

        /// <summary>
        /// Rows of range. (minimum is 1)
        /// </summary>
        private int _rows;

        /// <summary>
        /// Rows of range. (minimum is 1)
        /// </summary>
        public int Rows
        {
            get
            {
                return this._rows;
            }
        }

        #endregion Rows of range.

        #region Columns of range

        /// <summary>
        /// Columns of range. (minimum is 1)
        /// </summary>
        private int _columns;

        /// <summary>
        /// Columns of range. (minimum is 1)
        /// </summary>
        public int Columns
        {
            get
            {
                return this._columns;
            }
        }

        #endregion Columns of range

        #region The end row of range

        /// <summary>
        /// The end row of range
        /// </summary>
        public int Row2
        {
            get
            {
                return this._row + this._rows - 1;
            }
        }

        #endregion The end row of range

        #region The end column of range

        /// <summary>
        /// The end column of range
        /// </summary>
        public int Column2
        {
            get
            {
                return this._column + this._columns - 1;
            }
        }

        #endregion The end column of range

        #region Constructors

        /// <summary>
        /// Construct range with specified start-location and end-location.
        /// If the end-location is less than start-location, the correct location
        /// will be fixed automatically.
        /// </summary>
        /// <param name="startPos">Start location of range</param>
        /// <param name="endPos">End location of range</param>
        public SheetRange(int startRow, int startColumn, int endRow, int endColumn)
        {
            this._row = Math.Min(startRow, endRow);
            this._column = Math.Min(startColumn, endColumn);
            this._rows = Math.Max(startRow, endRow) - this._row + 1;
            this._columns = Math.Max(startColumn, endColumn) - this._column + 1;
        }

        #endregion Constructors
    }
}