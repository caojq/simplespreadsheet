﻿namespace SimpleSpreadsheet.Control
{
    /// <summary>
    /// Cell object for sheet control
    /// </summary>
    public class SheetCell
    {
        #region Position

        /// <summary>
        /// Row of cell
        /// </summary>
        private int _row;

        /// <summary>
        /// Row of cell
        /// </summary>
        public int Row
        {
            get
            {
                return this._row;
            }
            set
            {
                this._row = value;
            }
        }


        /// <summary>
        /// Colnum of cell
        /// </summary>
        private int _colnum;

        /// <summary>
        /// Colnum of cell
        /// </summary>
        public int Colnum
        {
            get
            {
                return this._colnum;
            }
            set
            {
                this._colnum = value;
            }
        }

        #endregion Position

        #region Data & Display

        public string DataFormat
        {
            get
            {
                return "{0, 3}";
            }
        }

        /// <summary>
        /// Data of cell
        /// </summary>
        private int? _value;

        /// <summary>
        /// Get or set the data
        /// </summary>
        public int? Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
            }
        }

        /// <summary>
        /// Get the text for display
        /// </summary>
        public string Text
        {
            get
            {
                if (this._value.HasValue)
                {
                    return string.Format(this.DataFormat, this._value.Value);
                }
                else
                {
                    return string.Format(this.DataFormat, string.Empty);
                }
            }
        }

        /// <summary>
        /// Creates and returns a string representation of the current cell.
        /// </summary>
        /// <returns>A string representation of the current cell.</returns>
        public override string ToString()
        {
            return this.Text;
        }

        #endregion Data & Display
    }
}