﻿using System;
using System.Collections.Generic;

namespace SimpleSpreadsheet.Control
{
    /// <summary>
    /// The specified exception of spreadSheet
    /// </summary>
    public class SheetException : Exception
    {
        /// <summary>
        /// Error num
        /// </summary>
        private int _errNum = 0;

        /// <summary>
        /// Error message list
        /// </summary>
        private static Dictionary<int, string> ErrorMsgList = null;

        /// <summary>
        /// get error num
        /// </summary>
        public int ErrNum
        {
            get
            {
                return this._errNum;
            }
        }

        /// <summary>
        /// Initializes a new instance of the SheetException class with a specified error message.
        /// </summary>
        /// <param name="msg"></param>
        public SheetException(int errNum)
            : base(SheetException.GetMessage(errNum))
        {
            this._errNum = errNum;
        }

        private static string GetMessage(int errNum)
        {
            if (SheetException.ErrorMsgList == null)
            {
                SheetException.InitErrorMsg();
            }

            string msg = string.Empty;
            if (SheetException.ErrorMsgList.TryGetValue(errNum, out msg))
            {
                return msg;
            }
            else
            {
                return "The error num has not been defined";
            }
        }

        /// <summary>
        /// Init message list
        /// </summary>
        private static void InitErrorMsg()
        {
            SheetException.ErrorMsgList = new Dictionary<int, string>() {
                { -999, "The rows or columns is less than 1."},
                { -998, "The index of row or column is out of range."},
                { -997, "The index of row is out of range."},
                { -996, "The index of column is out of range."},
                { -995, "Please create a sheet first."},
                { -900, "Your command is not defined or command parameters is less than minimum count."}
            };
        }
    }
}