﻿using SimpleSpreadsheet.Control;
using System;
using System.Text.RegularExpressions;

namespace SimpleSpreadsheet
{
    public class Program
    {
        /// <summary>
        /// Spread sheet
        /// </summary>
        public static SpreadSheet Sheet { get; set; }

        #region The entry point of the application

        /// <summary>
        /// The entry point of the application
        /// </summary>
        public static int Main()
        {
            // Wait the user input command
            while (true)
            {
                try
                {
                    // Print command infomain
                    Console.WriteLine("This app is Simple Spreadsheet,");
                    Console.WriteLine("You can enter the following commands:");
                    Console.WriteLine("1. Create a new spread: C w h");
                    Console.WriteLine("2. Insert a number in specified cell (x1,y1): N x1 y1 v1");
                    Console.WriteLine("3. Sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3: S x1 y1 x2 y2 x3 y3");
                    Console.WriteLine("4. Quit: Q");

                    // Print the messange for user input
                    Console.WriteLine("Plese input the command:");

                    // Read the command:
                    string inputCmd = string.Empty;
                    while (string.IsNullOrEmpty(inputCmd))
                    {
                        inputCmd = Console.ReadLine();
                        inputCmd = inputCmd.Trim();
                    }
                    
                    // running the input command
                    if (CommandToRun(inputCmd) == CmdResult.Exit)
                    {
                        return 0;
                    }
                }

                // SpreadSheet Exception
                catch (SheetException sex)
                {
                    Console.WriteLine(string.Format("Appication Info: {0}", sex.Message));
                    Console.WriteLine("Please press any key to continue...");
                    Console.ReadKey();
                }

                // System error, exit this application.
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("System error: {0}", ex.Message));
                    return -999;
                }
            }
        }

#endregion The entry point of the application

#region running the input command

        /// <summary>
        /// running the input command
        /// </summary>
        /// <param name="inputCmd">the command for running</param>
        public static CmdResult CommandToRun(string inputCmd)
        {
            // Change the command to uppercase
            inputCmd = inputCmd.ToUpper();

            // Check the command format is right ?
            var checkResult = Regex.IsMatch(inputCmd, @"^((C(\s+\d+){2,})|(N(\s+\d+){3,})|(S(\s+\d+){6,})|(Q.*))$");

            // the command is not defined or command parameters is less than minimum count
            if (!checkResult)
            {
                // print warning message and wait a new command
                throw new SheetException(-900);
            }

            // Split the command by space
            var cmdList = Regex.Split(inputCmd, @"\s+", RegexOptions.IgnoreCase);

            switch (cmdList[0])
            {
                // Should create a new spread sheet of width w and height h (i.e. the spreadsheet can hold w * h amount of cells)
                case "C":

                    Program.Sheet = SpreadSheet.CreateSheet(int.Parse(cmdList[2]), int.Parse(cmdList[1]));

                    break;

                // Should insert a number in specified cell (x1,y1)
                case "N":

                    if (Program.Sheet == null)
                    {
                        throw new SheetException(-995);
                    }

                    // Insert the data into cell
                    Program.Sheet[int.Parse(cmdList[2]), int.Parse(cmdList[1])] = int.Parse(cmdList[3]);

                    break;

                // Should perform sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3
                case "S":

                    if (Program.Sheet == null)
                    {
                        throw new SheetException(-995);
                    }

                    // get sum result of the range(From [x1, y1] to [x2, y2])
                    var sumResult = Program.Sheet.SumRange(int.Parse(cmdList[2]), int.Parse(cmdList[1]), int.Parse(cmdList[4]), int.Parse(cmdList[3]));

                    // Insert the sum result into result cell([x3, y3])
                    Program.Sheet[int.Parse(cmdList[6]), int.Parse(cmdList[5])] = sumResult;

                    break;

                // Should quit the program
                case "Q":

                    // Exit this application
                    return CmdResult.Exit;
            }

            // print the sheet info
            Console.WriteLine();
            Console.WriteLine(Program.Sheet.ToString());

            // Command run result is OK
            return CmdResult.Normal;
        }

#endregion running the input command
    }

    public enum CmdResult
    {
        /// <summary>
        /// Command run result is OK
        /// </summary>
        Normal,

        /// <summary>
        /// Exit this application
        /// </summary>
        Exit
    }
}